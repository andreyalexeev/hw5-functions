"use strict";

// task 1

function divide(a, b) {
    if (b === 0) {
        console.log("Помилка: Ділення на нуль неможливе.");
        return;
    }
    return a / b;
}

let numerator = 10;
let denominator = 2;

let result = divide(numerator, denominator);
console.log(`Частка ${numerator} на ${denominator} дорівнює ${result}.`);

// task 2

// function validateNumberInput(promptMessage) {
//     let userInput;
//     do {
//         userInput = prompt(promptMessage);
//     } while (isNaN(userInput));
//     return parseFloat(userInput);
// }

// function performOperation(a, b, operation) {
//     switch (operation) {
//         case '+':
//             return a + b;
//         case '-':
//             return a - b;
//         case '*':
//             return a * b;
//         case '/':
//             if (b === 0) {
//                 alert('Ділення на нуль неможливе.');
//                 return NaN;
//             }
//             return a / b;
//         default:
//             alert('Такої операції не існує.');
//             return NaN;
//     }
// }

// let firstNumber = validateNumberInput('Введіть перше число:');
// let secondNumber = validateNumberInput('Введіть друге число:');
// let operation = prompt('Введіть математичну операцію (+, -, *, /):');

// let result = performOperation(firstNumber, secondNumber, operation);
// if (!isNaN(result)) {
//     console.log(`Результат: ${result}`);
// }

// task 3

// const calculateFactorial = (number) => {
//     if (number < 0) {
//         return "Факторіал від'ємного числа не визначений.";
//     } else if (number === 0 || number === 1) {
//         return 1;
//     } else {
//         let factorial = 1;
//         for (let i = 2; i <= number; i++) {
//             factorial *= i;
//         }
//         return factorial;
//     }
// };

// const userInput = prompt("Введіть число для обчислення факторіалу:");
// const number = parseFloat(userInput);

// const result = calculateFactorial(number);
// console.log(`Факторіал числа ${number} дорівнює ${result}.`);


