1) Є три вида функцій: function declaration, function expression та стрілкова функція.
Створити функцію (function declaration):

function sayHello() {
    console.log('Привіт, світ!');
}

sayHello(); // викликаємо функцію

2) Оператор return в JavaScript використовується в функціях для повернення даних після виконання роботи функції.
Приклад використання оператора return в функції:

function square(number) {
    return number * number;
}

let result = square(3); 

3) Параметри та аргументи в функціях — це важливі поняття, які допомагають передавати дані між функціями та використовувати їх для обчислень.

Приклад:

function multiply(a, b) {  
    return a * b;
}

let result = multiply(3, 4); // Тут 3 та 4 — аргументи, які передаються в параметри a та b
console.log(result); // 

4) function greet(name) {
    console.log(`Привіт, ${name}!`);
}

function processUserInput(callback) {
    let userName = prompt("Введіть ваше ім'я:");
    callback(userName);
}

processUserInput(greet);




